# Get Bearer Token from Auth0 to dev envirienment
curl --request POST \
  --url https://dev-5qavkts8.us.auth0.com/oauth/token \
  --header 'content-type: application/json' \
  --data '{"client_id":"3jY4kLZTaKq8aGpZrYvQ29V6Urmc3QVM","client_secret":"4rAjCDzBFwBfehcY3F7fpcDo9lSaObAhej0PbJHmOVxCfCnCaXZWOXB3RYA33Qd5","audience":"http://localhost:3003/","grant_type":"client_credentials"}'

# Run logs
sudo heroku logs --tail --app ppm-gestao-normas-tecnicas-api


# Run Migrations
heroku run 'node ./node_modules/typeorm/cli.js migration:run' --app ppm-gestao-normas-tecnicas-api

# Replace ormconfig.js to run migration on prod enviroment
module.exports = {
    "type": "postgres",
    "url":"postgres://odxxurwtbfzgff:ea8442bcc181a93e342e46b5111d44f05fe638d75730c94c39cf850b848e2795@ec2-34-194-14-176.compute-1.amazonaws.com:5432/dd2cgutbpjjrv2",
    "migrations": ["./dist/infrastructure/dbconfig/migrations/*.js"],
    "entities": ["./dist/infrastructure/entities/*.js"],
    "cli": {
        "migrationsDir": "./src/infrastructure/dbconfig/migrations/"
    },
    "ssl": { rejectUnauthorized: false }
}

# Replace ormconfig.js to run migration on dev enviroment
module.exports = {
    "type": "postgres",
    "url":"postgres://postgres:postgres@localhost:5432/normas_tecnicas",
    "migrations": ["./src/infrastructure/dbconfig/migrations/*.ts"],
    "entities": ["./src/infrastructure/entities/*.ts"],
    "cli": {
        "migrationsDir": "./src/infrastructure/dbconfig/migrations/"
    },
    "ssl": false
}