import 'reflect-metadata';
import './env/envconfig';
import express                  from 'express';
import cors                     from 'cors';
import jwtCheck                 from './ui/authconfig'
import router                   from './ui/controllers/routes';
import createDbConnection       from "./infrastructure/dbconfig/typeormconfig";
import setupSwagger             from './ui/swagger/swaggerconfig';

const port =  process.env.PORT || 3003;

const api = express();
api.use(express.json());
api.use(cors());
setupSwagger(api);
api.use("/api", router);
api.use(jwtCheck)
createDbConnection(api, port);