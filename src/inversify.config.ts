import TYPES                        from "./domain/constants/types";
import { Container }                from "inversify";
import { ITechnicalNormRepository } from "./domain/interfaces/repositories/technicalnorm.repository.interface";
import { ITechnicalNormService }    from "./domain/interfaces/services/technicalnorm.service.interface";
import { TechnicalNormService }     from "./domain/services/technicalnorm.service";
import { TechnicalNormRepository }  from "./infrastructure/repositories/technicalnorm.repository"

const container = new Container();
container.bind<ITechnicalNormRepository>(TYPES.TechnicalNormRepository).to(TechnicalNormRepository);
container.bind<ITechnicalNormService>(TYPES.TechnicalNormService).to(TechnicalNormService);

export default container;