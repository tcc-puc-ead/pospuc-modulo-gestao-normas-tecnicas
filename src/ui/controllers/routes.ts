import { Router } from "express";
import { TechNormController } from "./technicalnorm.controller";

const router = Router();
const controller = new TechNormController()

router.get('/technorms', controller.listNorms);
router.get('/technorms/:id', controller.findNorm);
router.get('/technormsbytitle/:title', controller.findNormByTitle);
router.post('/technorm',  controller.createTechNorm);
router.put('/technorm', controller.updateTechNorm);
router.delete('/technorm/:id', controller.deleteTechNorm);

export default router;
