import { Response } from "express";
import { AppError } from "../../domain/shared/apperror";

const middlewareController = (error: Error | any, response: Response) => {

  if (error instanceof AppError) {
    return response.status(error.statusCode).json({
      status: error.statusCode,
      message: error.message,
    });
  }

  return response.status(500).json({
    status: 500,
    message: `Internal server error. Inner: ${error.message}`,
  });
}  


export {middlewareController}
  
  