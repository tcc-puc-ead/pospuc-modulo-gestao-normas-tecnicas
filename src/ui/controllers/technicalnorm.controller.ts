import { Request, Response }        from 'express';
import container                    from '../../inversify.config';
import TYPES                        from '../../domain/constants/types';
import { ITechnicalNormService }    from '../../domain/interfaces/services/technicalnorm.service.interface';
import { TechnicalNormModel }       from '../../domain/models/technicalnorm.model';
import { middlewareController } from './middleware.controller.';


export class TechNormController {

_service:ITechnicalNormService = container.get<ITechnicalNormService>(TYPES.TechnicalNormService);

createTechNorm = async (req: Request, res: Response) => {
    
    try{
        let {normTitle, normBody, competentOrganization} = req.body;

        let model = new TechnicalNormModel("", normTitle, normBody, competentOrganization);
        let modelresponse = await this._service.create(model);

        res.json(modelresponse);
    }
    catch(error){
        middlewareController(error, res)
    }
}


updateTechNorm = async (req: Request, res: Response) => {

    try{
        let {normId, normTitle, competentOrganization, normBody} = req.body;

        let model = new TechnicalNormModel(normId, normTitle, normBody, competentOrganization);
        let modelresponse = await this._service.update(model);

        res.json(modelresponse);
    }
    catch(error){
        middlewareController(error, res)
    }
};

deleteTechNorm = async (req: Request, res: Response) => {
    
    try{
        let { id } = req.params;
        await this._service.delete(id);

        res.json({message: "Norma deletada com sucesso"});
    }
    catch(error){
        middlewareController(error, res)
    }
    
};

findNorm = async (req: Request, res: Response) => {

    try{
        let { id } = req.params;

        let model = await this._service.find(id);

        res.json(model);
    }
    catch(error){
        middlewareController(error, res)
    }
};

findNormByTitle = async (req: Request, res: Response) => {

    try{
        let { title } = req.params;

        let model = await this._service.findByTitle(title);

        res.json(model);
    }
    catch(error){
        middlewareController(error, res)
    }
};

listNorms = async (req: Request, res: Response) => {
    try{
        console.log(req);

        let models = await this._service.findAll();

        res.json(models);
    }
    catch(error){
        middlewareController(error, res)
    }  
}
}
