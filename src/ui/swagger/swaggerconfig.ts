import  * as swaggerUi  from "swagger-ui-express";
import * as path        from "path";
import * as fs          from "fs";
import {Express}        from 'express';

/* Swagger files start */
const swaggerFile: any = path.resolve(__dirname,"swagger.json");
const swaggerData: any = fs.readFileSync(swaggerFile, 'utf8');
const customCss: any = fs.readFileSync(path.resolve(__dirname, "swagger.css"), 'utf8');
const swaggerDocument = JSON.parse(swaggerData);

swaggerDocument.host = process.env.BASE_URL
swaggerDocument.schemes.push(process.env.SCHEME)
/* Swagger files end */

const options = {
    swaggerOptions: {
      authAction :{ JWT: {name: "JWT", schema: {type: "apiKey", in: "header", name: "Authorization", description: ""}, value: "Bearer <JWT>"} }
    }
  };

const setupSwagger = (api: Express) => {
    api.use("/docs", swaggerUi.serve, 
          swaggerUi.setup(swaggerDocument, options, options, customCss));
}

export default setupSwagger;