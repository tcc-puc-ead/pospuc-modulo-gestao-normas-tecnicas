
import jwt      from 'express-jwt';
import jwks     from 'jwks-rsa';

const jwtURI:string = process.env.JWT_URI ? process.env.JWT_URI : "";

const jwtCheck = jwt({
    secret: jwks.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: jwtURI
  }),
  audience: process.env.JWT_AUDIENCE,
  issuer: process.env.JWT_ISSUE,
  algorithms: ['RS256']
  }).unless({path: ["/docs"]});


  export default jwtCheck