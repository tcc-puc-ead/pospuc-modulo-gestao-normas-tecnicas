const uuidExp = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
const niluuidExp = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;

const isUUIDValid = (value:string) => {
    return uuidExp.test(value) || niluuidExp.test(value); 
}

const isEmpty = (value: string) => {
    return value == "" || value === null || value === undefined;
}


export  { isEmpty, isUUIDValid }