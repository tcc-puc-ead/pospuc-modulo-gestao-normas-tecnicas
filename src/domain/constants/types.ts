const TYPES = {
    TechnicalNormService: Symbol("TechnicalNormService"),
    TechnicalNormRepository: Symbol("TechnicalNormRepository")
}

export default TYPES;