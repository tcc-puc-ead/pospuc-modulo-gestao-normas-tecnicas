import "reflect-metadata";
import { injectable }               from "inversify";
import TYPES                        from "../constants/types";
import container                    from "../../inversify.config";
import { isEmpty, isUUIDValid }              from "../shared/commom";
import { ITechnicalNorm }           from "../interfaces/models/technicalnorm.model.interface";
import { ITechnicalNormService }    from "../interfaces/services/technicalnorm.service.interface";
import { ITechnicalNormRepository } from "../interfaces/repositories/technicalnorm.repository.interface";
import { AppError } from "../shared/apperror";

@injectable()
export class TechnicalNormService implements ITechnicalNormService{
    repository: ITechnicalNormRepository;
    
    constructor(){
        this.repository = container.get<ITechnicalNormRepository>(TYPES.TechnicalNormRepository);
    }

    public async find(id: string): Promise<ITechnicalNorm> {

        this.uuidValid(id);
        
        return await this.repository.findByID(id);
    }

    public async findAll(): Promise<ITechnicalNorm[]> {
        return await this.repository.findAll();
    }

    public async findByTitle(title:string): Promise<ITechnicalNorm[]> {
        console.log(title);
        if(isEmpty(title)){
            throw new AppError("O titulo da norma deve ser informado");
        }

        return await this.repository.findByTitle(title);
        
    }

    public async create(model: ITechnicalNorm): Promise<ITechnicalNorm> {
        
        model.validModel();
        return await this.repository.create(model);
    }

    public async update(model: ITechnicalNorm): Promise<ITechnicalNorm> {
        await this.repository.update(model);
        return await this.repository.findByID(model.normId);
    }

    public async delete(id: string): Promise<void> {

        this.uuidValid(id);

        await this.repository.delete(id);
    }

    uuidValid(id: string){
        if(!isUUIDValid(id)){
            throw new AppError("ID não está em formato valido");
        }
    }
    
}

