export interface ITechnicalNorm{
    normId: string;
    normTitle: string;
    normBody: string;
    competentOrganization: string;

    validModel():void;
}