import { ITechnicalNorm } from "../models/technicalnorm.model.interface";

 
export interface ITechnicalNormRepository{
    create(model: ITechnicalNorm): Promise<ITechnicalNorm>
    update(model: ITechnicalNorm): Promise<void>
    delete(id: string): Promise<void>
    findByTitle(title: string): Promise<ITechnicalNorm[]>
    findByID(id: string): Promise<ITechnicalNorm>
    findAll(): Promise<ITechnicalNorm[]>
}