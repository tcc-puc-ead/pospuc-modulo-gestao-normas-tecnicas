import { ITechnicalNorm } from "../models/technicalnorm.model.interface";
import { ITechnicalNormRepository } from "../repositories/technicalnorm.repository.interface";
import { IGenericService } from "./genericservice.interface";

export interface ITechnicalNormService extends IGenericService<ITechnicalNorm>{

    repository: ITechnicalNormRepository;

    findByTitle(title:string): Promise<ITechnicalNorm[]>;
}