import { ITechnicalNorm } from "../models/technicalnorm.model.interface";

export interface IGenericService<T> {
    create(item:T) : Promise<ITechnicalNorm>;
    update(item:T) : Promise<ITechnicalNorm>;
    delete(id: string) : Promise<void>;
    find(id: string) : Promise<ITechnicalNorm>;
    findAll(): Promise<ITechnicalNorm[]>;
}