import { isEmpty } from "../shared/commom";
import { ITechnicalNorm } from "../interfaces/models/technicalnorm.model.interface";
import { AppError } from "../shared/apperror";


export class TechnicalNormModel implements ITechnicalNorm{
    
    normId: string;
    normTitle: string;
    normBody: string;
    competentOrganization: string;

    constructor(id: string, title: string, body: string, organization: string){
        this.normId = id;
        this.normTitle = title;
        this.competentOrganization = organization;
        this.normBody = body;
    }
    

    validModel():void{
        if(isEmpty(this.normTitle) || isEmpty(this.normBody) || isEmpty(this.normTitle)){
            throw new AppError("Model invalido", 400)
        }
    }
    
}