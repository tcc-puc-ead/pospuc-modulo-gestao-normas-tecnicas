import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateTechNorm1628249760452 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: "normas_tecnicas",
            columns: [
                {
                    name: 'id',
                    type: 'uuid',
                    isPrimary: true,
                    generationStrategy: 'uuid',
                    default: 'uuid_generate_v4()'
                },
                {
                    name: 'titulo',
                    type: 'varchar',
                },
                
                {
                    name: 'data_criacao',
                    type: 'timestamp',
                    default: 'now()',
                },
                {
                    name: 'data_atualizacao',
                    type: 'timestamp',
                    default: 'now()',
                },
            ]
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("normas_tecnicas")
    }

}
