import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateOtherColunsTechNorm1628813576500 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "normas_tecnicas" ADD "corpo_norma" TEXT NOT NULL`);
        await queryRunner.query(`ALTER TABLE "normas_tecnicas" ADD "orgao_competente" VARCHAR(256) NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "normas_tecnicas" DROP "corpo_norma"`);
        await queryRunner.query(`ALTER TABLE "normas_tecnicas" DROP "orgao_competente"`);
    }

}
