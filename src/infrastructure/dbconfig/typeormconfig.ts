import { Application } from "express";
import { createConnection } from "typeorm";

const createDbConnection = (api: Application, port:any) => {
    createConnection().then(() => {
        api.listen(port, () => console.log('listen to port: ' + port));
    }).catch((error) => {
        console.log(error);
        console.log(error.message);
    });
}

export default createDbConnection;