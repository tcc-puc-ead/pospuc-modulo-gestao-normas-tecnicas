import {Column, CreateDateColumn, Entity, 
        PrimaryGeneratedColumn,UpdateDateColumn} from 'typeorm';


@Entity('normas_tecnicas')
export class TechnicalNormEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    titulo: string;

    @Column()
    corpo_norma: string;

    @Column()
    orgao_competente: string;

    @CreateDateColumn()
    data_criacao: Date;

    @UpdateDateColumn()
    data_atualizacao: Date;

    constructor(id:string, titulo: string, corpo_norma: string, orgao_competente: string){
        this.id = id;
        this.titulo = titulo;
        this.corpo_norma = corpo_norma;
        this.orgao_competente = orgao_competente;
    }
}