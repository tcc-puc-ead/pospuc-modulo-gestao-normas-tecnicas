import "reflect-metadata";
import { injectable }                                       from "inversify";
import { Like, EntityRepository, getCustomRepository, Repository} from 'typeorm';
import { ITechnicalNormRepository }                         from "../../domain/interfaces/repositories/technicalnorm.repository.interface";
import { TechnicalNormEntity }                              from "../entities/TechnicalNormEntity";
import { ITechnicalNorm } from "../../domain/interfaces/models/technicalnorm.model.interface";
import { TechnicalNormMap } from "../modelmap/technicalnorm.modalmap";
import { TechnicalNormModel } from "../../domain/models/technicalnorm.model";


@EntityRepository(TechnicalNormEntity)
class TechnicalNormDbRepository extends Repository<TechnicalNormEntity>{

}

@injectable()
export class TechnicalNormRepository implements ITechnicalNormRepository
{
    private modelMap: TechnicalNormMap;

    public constructor(){
        this.modelMap = new TechnicalNormMap();
    }

    public async create(model: ITechnicalNorm): Promise<ITechnicalNorm> {
        let repository = this.getRepository();
        
        let entity: TechnicalNormEntity = repository.create({titulo: model.normTitle, 
                                                            orgao_competente: model.competentOrganization,
                                                            corpo_norma: model.normBody});
        await repository.save(entity);

        return this.modelMap.MapEntityToModel(entity);
    }
    
    public async update(model: ITechnicalNorm): Promise<void> {
        let repository = this.getRepository();

        let entity :TechnicalNormEntity = this.modelMap.MapModelToEntity(model);

        await repository.save(entity);
    }

    public async delete(id: string): Promise<void> {
            
        let repository = this.getRepository();

        await repository.delete({"id": id});
    }

    public async findByTitle(title: string): Promise<ITechnicalNorm[]> {
        
        let repository = this.getRepository();
        
        let entities = await repository.find({"titulo": Like(`%${title}%`)})
        
        return this.getTechNormList(entities);
    }

    public async findByID(id: string): Promise<ITechnicalNorm> {
        let repository = this.getRepository();
        
        let entity = await repository.findOne(id);
        
        if(entity){
            return this.modelMap.MapEntityToModel(entity);
        }

        return new TechnicalNormModel('','','','');
    }

    public async findAll(): Promise<ITechnicalNorm[]>{
        let repository = this.getRepository();
        
        let entities = await repository.find();
        
        if(!entities || entities.length == 0)
            return [];

        return this.getTechNormList(entities);
    }
    
    private getRepository():TechnicalNormDbRepository{
        return getCustomRepository(TechnicalNormDbRepository); 
    }

    private getTechNormList(entities:TechnicalNormEntity[]):ITechnicalNorm[]{
        let models: ITechnicalNorm[] = [];
        
        entities.forEach((entity) => {
            models.push(this.modelMap.MapEntityToModel(entity))
        });
        
        return models;
    }
}