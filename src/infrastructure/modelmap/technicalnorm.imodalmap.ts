export interface GenericModalMap<TEntity, TModel>{
    MapEntityToModel(entity: TEntity): TModel | undefined;
    MapModelToEntity(model: TModel): TEntity | undefined;
}