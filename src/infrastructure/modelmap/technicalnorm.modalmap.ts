import { TechnicalNormModel } from "../../domain/models/technicalnorm.model";
import { AppError } from "../../domain/shared/apperror";
import { TechnicalNormEntity } from "../entities/TechnicalNormEntity";
import { GenericModalMap } from "./technicalnorm.imodalmap";

export class TechnicalNormMap implements GenericModalMap<TechnicalNormEntity, TechnicalNormModel>{
    
    MapEntityToModel(entity: TechnicalNormEntity): TechnicalNormModel {
        
        if(!entity){
            throw new AppError("entidade para transformação está nulo");
        }

        return new TechnicalNormModel(entity.id, entity.titulo, entity.corpo_norma, entity.orgao_competente);
    }

    MapModelToEntity(model: TechnicalNormModel): TechnicalNormEntity {
        
        if(!model){
            throw new AppError("model para transformação está nulo");
        }

        return new TechnicalNormEntity(model.normId, model.normTitle, model.normBody, model.competentOrganization);
        
    }

}