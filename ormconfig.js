module.exports = {
    "type": "postgres",
    "url":process.env.DB_URL,
    "migrations": [process.env.DB_MIGRATIONS],
    "entities": [process.env.DB_ENTITIES],
    "cli": {
        "migrationsDir": "./src/infrastructure/dbconfig/migrations/"
    },
    "ssl": process.env.NODE_ENV === 'production' ? { rejectUnauthorized: false } : false
}
